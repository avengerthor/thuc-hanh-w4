import 'widgets/header.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final tabs = [
    Header(),
    Container(
      child: Center(
        child: Text(
          'Shopee Feed',
          style: TextStyle(fontSize: 30),
        ),
      ),
    ),
    Container(
      child: Center(
        child: Text(
          'Live',
          style: TextStyle(fontSize: 30),
        ),
      ),
    ),
    Container(
      child: Center(
        child: Text(
          'Thông Báo',
          style: TextStyle(fontSize: 30),
        ),
      ),
    ),
    Container(
      child: Center(
        child: Text(
          'Tôi',
          style: TextStyle(fontSize: 30),
        ),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    // return Scaffold(
    //   backgroundColor: Colors.deepOrange,
    //   body: Header(),
    // );
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.deepOrange,
        body: tabs[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          fixedColor: Colors.black,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Home",
              // backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.dynamic_feed_outlined),
              label: "Shopee Feed",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.video_camera_back_sharp),
              label: "Live",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications_active),
              label: "Thông báo",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Tôi",
              backgroundColor: Colors.red,
            ),
          ],
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
      ),
    );
  }
}
